<!DOCTYPE HTML>
<html>  
<body>

<?php
   
   require_once('animal.php');
   require('frog.php');
   require('ape.php');

   $sheep = new Animal("shaun");

    echo "Nama binatang : " ;
    echo $sheep->name; // "shaun"
    echo "<br>";
    echo "Jumlah kaki : ";
    echo $sheep->legs; // 4
    echo " <br>";
    echo "Cold blooded : ";
    echo $sheep->cold_blooded; // "no"
    echo " <br>";

    $sungokong = new Ape("kera sakti");
    

    echo " <br>";
    echo "Nama binatang : " ;
    echo $sungokong->name;
    echo " <br>";
    echo "Jumlah kaki : ";
    echo $sungokong->legs;
    echo " <br>";
    echo "Yell : ";
     $sungokong->yell();
    echo " <br>";
    echo "Cold blooded : ";
    echo $sungokong->cold_blooded;
    echo " <br>";

    $kodok = new Frog("buduk");
    
    
    echo " <br>";
    echo "Nama binatang : " ;
    echo $kodok->name;
    echo " <br>";
    echo "Jumlah kaki : ";
    echo $kodok->legs;
    echo " <br>";
    echo "Jump : ";
     $kodok->jump();
    echo " <br>";
    echo "Cold blooded : ";
    echo $kodok->cold_blooded;


?></span>
  



</body>
</html>